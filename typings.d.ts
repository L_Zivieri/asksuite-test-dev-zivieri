
export type RoomReservationInformation = {
    "hotel": string,
    "name": string,
    "description": string,
    "image": string,
    "paymentOptions": {
        "description": string,
        "price": string,
    }[],
    "$reference": string,
}
