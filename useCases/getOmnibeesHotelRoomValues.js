const CrawlerService = require("../services/CrawlerService");

/**
 * @param {{
 *      checkInDate: Date,
 *      checkOutDate: Date,
 *      numberOfAdults: number,
 *      numberOfKids: number,
 * }} 
 * @returns {Promise<import("../typings").RoomReservationInformation[]>}
 */
function getOmnibeesHotelRoomValues({ checkInDate, checkOutDate, numberOfAdults, numberOfKids }) {
    const checkInDateValue = checkInDate.toJSON().replace(/^([0-9]{4})-([0-9]{2})-([0-9]{2}).*/, "$3$2$1");
    const checkOutDateValue = checkOutDate.toJSON().replace(/^([0-9]{4})-([0-9]{2})-([0-9]{2}).*/, "$3$2$1");

    const informationConstruction = {
        selector: "#step-results-center #hotels_grid > div:not(.error-div):not(.d-none)",
        list: true,
        childrens: [
            {
                key: "name",
                selector: ".hotel_name",
            },
            {
                key: "description",
                selector: ".hotel-description",
            },
            {
                key: "image",
                attr: "attr.src",
                selector: "img.image-step2",
            },
            {
                key: "paymentOptions",
                list: true,
                selector: ".rate_plan",
                childrens: [
                    {
                        key: "description",
                        selector: ".rate_name .t-tip__text span"
                    },
                    {
                        key: "price",
                        selector: ".price-total"
                    }
                ]
            }
        ]
    }

    /**
     * @param {string} hotelName 
     * @param {string} url 
     * @returns {Promise<import("../typings").RoomReservationInformation[]>}
     */
    const crawl = async (hotelName, url) => {
        return CrawlerService
            .scrapFromUrl(url, informationConstruction)
            .then(rooms => rooms.map(
                v => ({ hotel: hotelName, ...v, $reference: url })
            ));
    }

    return Promise.all([
        crawl(
            "Hotel Village Le Canton",
            `https://book.omnibees.com/hotelresults?c=2983&q=5462&hotel_folder=&NRooms=1&CheckIn=${checkInDateValue}&CheckOut=${checkOutDateValue}&ad=${numberOfAdults}&ch=${numberOfKids}&ag=-&group_code=&Code=AMIGODODANIEL&loyalty_code=&mob-code=on&lang=pt-BR&currencyId=`
        ),
        crawl(
            "Hotel Fazenda Suíça Le Canton",
            `https://book.omnibees.com/hotelresults?c=2983&q=5461&hotel_folder=&NRooms=1&CheckIn=${checkInDateValue}&CheckOut=${checkOutDateValue}&ad=${numberOfAdults}&ch=${numberOfKids}&ag=-&group_code=&Code=AMIGODODANIEL&loyalty_code=&mob-code=on&lang=pt-BR&currencyId=`,
        ),
        crawl(
            "Hotel Magique Le Canton",
            `https://book.omnibees.com/hotelresults?c=2983&q=8795&hotel_folder=&NRooms=1&CheckIn=${checkInDateValue}&CheckOut=${checkOutDateValue}&ad=${numberOfAdults}&ch=${numberOfKids}&ag=-&group_code=&Code=AMIGODODANIEL&loyalty_code=&mob-code=on&lang=pt-BR&currencyId=`,
        ),
    ]).then(hotelRooms => [].concat(...hotelRooms))
}
module.exports = getOmnibeesHotelRoomValues;