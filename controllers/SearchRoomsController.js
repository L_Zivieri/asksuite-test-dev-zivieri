const getOmnibeesHotelRoomValues = require("../useCases/getOmnibeesHotelRoomValues");

class SearchRoomsController {

    /**
     * This is the API request as the requested in README.md
     */
    static async getRooms(req, res) {
        const requestValues = Object.assign({}, req.query, req.body || {})

        const checkInDate = new Date(requestValues.checkin + "T12:00:00");
        const checkOutDate = new Date(requestValues.checkout + "T12:00:00");

        if (checkInDate == "Invalid Date") return res.status(422).json("Invalid check In date. Format needs to be YYYY-MM-DD");
        if (checkOutDate == "Invalid Date") return res.status(422).json("Invalid check Out date. Format needs to be YYYY-MM-DD");

        try {
            // Get all the rooms
            let rooms = await getOmnibeesHotelRoomValues({
                checkInDate,
                checkOutDate,
                numberOfAdults: 1,
                numberOfKids: 0,
            })

            // Check quantity of hotels collecteds to show to user which is selected
            let hotels = rooms.map(room => room.hotel);
            hotels = hotels.filter((h, i) => hotels.indexOf(h) == i)

            // Collapse payment options data with others datas
            rooms = rooms.map(room => {
                const hotelString = hotels.length != 1 ? `${room.hotel} - ` : ``
                if (room.paymentOptions.length == 1) {
                    return [{
                        "name": `${hotelString}${room.name}`,
                        "description": room.description,
                        "price": room.paymentOptions[0].price,
                        "image": room.image,
                    }]
                }
                return room.paymentOptions.map(po => ({
                    "name": `${hotelString}${room.name} - ${po.description}`,
                    "description": room.description,
                    "price": po.price,
                    "image": room.image,
                }));
            })

            // Collapse into only one array
            rooms = [].concat(...rooms)

            // Send the final data to users
            res.json(rooms)
        } catch (error) {
            res.status(500).json(error.message);
        }
    }

    /**
     * Another way to collect all the data from omnibees site.
     * With payment options and adult/kids quantities
     */
    static async getRoomsComplete(req, res) {
        const requestValues = Object.assign({}, req.query, req.body || {})

        const checkInDate = new Date(requestValues.checkin + "T12:00:00");
        const checkOutDate = new Date(requestValues.checkout + "T12:00:00");
        const numberOfAdults = requestValues.adults === undefined ? 1 : requestValues.adults;
        const numberOfKids = requestValues.kids === undefined ? 0 : requestValues.kids;

        if (checkInDate == "Invalid Date") return res.status(422).json("Invalid check In date. Format needs to be YYYY-MM-DD");
        if (checkOutDate == "Invalid Date") return res.status(422).json("Invalid check Out date. Format needs to be YYYY-MM-DD");
        if (+numberOfAdults != numberOfAdults) return res.status(422).json("Invalid adults number");
        if (+numberOfKids != numberOfKids) return res.status(422).json("Invalid kids number");

        try {
            res.json(
                await getOmnibeesHotelRoomValues({
                    checkInDate,
                    checkOutDate,
                    numberOfAdults,
                    numberOfKids,
                })
            )
        } catch (error) {
            res.status(500).json(error.message);
        }
    }
}
module.exports = SearchRoomsController