const express = require('express');
const router = express.Router();

const SearchRoomsController = require('../controllers/SearchRoomsController');

router.get('/', (req, res) => {
    res.send('Hello Asksuite World!');
});

router.post('/search', SearchRoomsController.getRooms);
router.get('/rooms', SearchRoomsController.getRoomsComplete);

module.exports = router;
