const BrowserService = require('./BrowserService');

/**
 * @typedef {Object} CrawlerToScrapData
 * @property {string} key
 * @property {string} selector
 * @property {string} attr Default: innerHTML
 * @property {boolean} list
 * @property {CrawlerToScrapData[] | undefined} childrens
 */

class CrawlerService {
    /**
     * @param {string} url 
     * @param {CrawlerToScrapData} toScrapData
     * @throws {CrawlerServiceScrapError} When the browser instance cannot be opened
     */
    static async scrapFromUrl(url, toScrapData) {

        /** @type {import("puppeteer").Browser} */
        let browser;

        /** @type {import("puppeteer").Page} */
        let page;

        try {
            browser = await BrowserService.getBrowser();
            page = await browser.newPage();
        } catch (error) {
            throw new CrawlerServiceScrapError("SCRAP_ERROR_CREATE_BROWSER_ERROR", error.message);
        }

        try {
            await page.goto(url, { waitUntil: 'networkidle0' });
            await page.setViewport({ width: 1440, height: 744 });
        } catch (error) {
            throw new CrawlerServiceScrapError("SCRAP_ERROR_URL_ERROR", error.message);
        }

        let data;

        try {
            const body = await page.$("html");
            data = await this.scrapItems(body, toScrapData);
        } catch (error) {
            throw new CrawlerServiceScrapError("SCRAP_ERROR_URL_ERROR", error.message);
        }

        BrowserService.closeBrowser(browser);

        return data;
    }

    /**
     * 
     * @param {import("puppeteer").ElementHandle<Element>} content 
     * @param {CrawlerToScrapData} scrapData 
     */
    static async scrapItems(content, scrapData) {
        let elements = !scrapData.selector ? [content] : await content.$$(scrapData.selector);

        if (!scrapData.list && !scrapData.childrens) {
            const values = await Promise.all(
                elements.map(async (element) =>
                    element.evaluate((el, attr) => {
                        let value;
                        if (attr.startsWith("attr.")) {
                            value = el.getAttribute(attr.substr(5))
                        } else {
                            value = el[attr]
                        }
                        return value;
                    }, scrapData.attr || "innerHTML")
                )
            )
            return values.map(v => v && v.replace(/^[\s\n]*([^\s\n].*[^\s\n])[\s\n]*$/g, "$1")).find(v => !!v)
        }

        if (scrapData.childrens) {
            elements = await Promise.all(
                elements.map(async (element) => {
                    try {
                        const finalValues = {};
                        for (const childrenScrapData of scrapData.childrens) {
                            if (childrenScrapData.childrens) {
                                finalValues[childrenScrapData.key] = await this.scrapItems(element, childrenScrapData);
                            } else {
                                finalValues[childrenScrapData.key] = await element.$eval(
                                    childrenScrapData.selector,
                                    (el, attr) => {
                                        let value;
                                        if (attr.startsWith("attr.")) {
                                            value = el.getAttribute(attr.substr(5))
                                        } else {
                                            value = el[attr]
                                        }
                                        return value && value.replace(/^[\s\n]*([^\s\n].*[^\s\n])[\s\n]*$/g, "$1");
                                    }, 
                                    childrenScrapData.attr || "innerHTML"
                                )
                            }
                        }
                        return finalValues;
                    } catch (error) {
                        console.error(error);
                        return null;
                    }
                })
            )
        }
        return elements.filter(v => !!v);
    };
}

module.exports = CrawlerService;

class CrawlerServiceScrapError extends Error {

    /**
     * @param {"SCRAP_ERROR_CREATE_BROWSER_ERROR" | "SCRAP_ERROR_URL_ERROR"} code 
     * @param {string} message 
     */
    constructor(code, message) {
        super(`[${code}] ${message}`);
    }
}
module.exports.CrawlerServiceScrapError = CrawlerServiceScrapError;
